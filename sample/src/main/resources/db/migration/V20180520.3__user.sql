create table user (
  id bigint primary key auto_increment,
  f_name varchar(255) not null,
  f_phone varchar(50) not null,
  create_time DATETIME DEFAULT NOW()
);