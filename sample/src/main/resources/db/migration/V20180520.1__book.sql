create table book(
  id bigint primary key auto_increment,
  name varchar(255) not null,
  content varchar(255) not null,
  num int default 0,
  shop_id bigint not null
);