create table shop (
  id bigint primary key auto_increment,
  name varchar(255) not null,
  address varchar(255) null,
  user_id bigint not null
);