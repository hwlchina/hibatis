package mybatis.generator.sample.entity;

import mybatis.generator.annotation.Column;
import mybatis.generator.annotation.Entity;
import mybatis.generator.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by dave on 18-5-20 上午11:36.
 */
@Entity(table = "user")
public class User implements Serializable {
    @Id
    private long id;
    @Column(column = "f_name")
    private String name;
    @Column(column = "f_phone")
    private String phone;
    private Date createTime = new Date();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
