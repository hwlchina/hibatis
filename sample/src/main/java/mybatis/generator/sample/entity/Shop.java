package mybatis.generator.sample.entity;

import mybatis.generator.FilterType;
import mybatis.generator.annotation.Entity;
import mybatis.generator.annotation.Filter;
import mybatis.generator.annotation.HasOne;
import mybatis.generator.annotation.Id;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dave on 18-5-20 上午11:30.
 */
@Entity(table = "shop")
public class Shop implements Serializable {
    @Id
    private long id;
    private String name;
    private String address;
    private long userId;
    @HasOne(view = "all", filter = @Filter(column = "id", type = FilterType.eq, placeholder = "#{userId}"))
    private List<User> user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
