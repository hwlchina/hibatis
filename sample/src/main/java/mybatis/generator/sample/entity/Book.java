package mybatis.generator.sample.entity;

import mybatis.generator.annotation.Entity;
import mybatis.generator.annotation.Id;

import java.io.Serializable;

/**
 * Created by dave on 18-5-20 上午10:48.
 */
@Entity(table = "book")
public class Book implements Serializable {
    @Id
    private long id;
    private String name;
    private String content;
    private int num;
    private long shopId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }
}
