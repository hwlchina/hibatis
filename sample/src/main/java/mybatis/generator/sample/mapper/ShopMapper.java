package mybatis.generator.sample.mapper;

import mybatis.generator.BaseMapper;
import mybatis.generator.sample.entity.Shop;
import mybatis.generator.sample.model.ShopQuery;
import mybatis.generator.annotation.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by dave on 18-5-20 上午10:44.
 */
@Mapper
public interface ShopMapper extends BaseMapper<Shop, Long> {
    @ExecuteInsert
    int insertMany(List<Shop> shop);

    @ExecuteSelect(view = "all")
    List<Shop> getList(@FilterParam ShopQuery query,
                       @FilterParam(table = "user", column = "f_name", join = @Join(table = "user", joinColumns =
                            @JoinColumn(column = "id", referColumn = "user_id", referTable = "shop")))
                           String username);

    @Update("truncate table shop")
    int clear();
}
