package mybatis.generator.sample.mapper;

import mybatis.generator.annotation.ExecuteInsert;
import mybatis.generator.annotation.ExecuteSelect;
import mybatis.generator.sample.entity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by dave on 18-5-20 上午10:44.
 */
@Mapper
public interface BookMapper {
    @ExecuteInsert
    int insert(Book book);

    @ExecuteSelect
    List<Book> getList();

    @Update("truncate table book")
    int clear();
}
