package mybatis.generator.sample.model;

import mybatis.generator.annotation.Filter;
import mybatis.generator.annotation.Limit;
import mybatis.generator.annotation.Offset;
import mybatis.generator.annotation.Sort;
import mybatis.generator.FilterType;

import java.util.List;

/**
 * Created by huangdachao on 2018/6/21 14:48.
 */
public class UserQuery {

    @Filter(type = FilterType.preMatch)
    private String name;

    @Filter(type = FilterType.preMatch)
    private String phone;

    @Filter(type = FilterType.in)
    private List<Long> id;

    @Limit
    private Integer limit;

    @Offset
    private Integer offset;

    @Sort
    private String sort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Long> getId() {
        return id;
    }

    public void setId(List<Long> id) {
        this.id = id;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
