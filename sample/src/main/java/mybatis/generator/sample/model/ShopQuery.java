package mybatis.generator.sample.model;

/**
 * Created by huangdachao on 2018/6/21 17:01.
 */
public class ShopQuery {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
