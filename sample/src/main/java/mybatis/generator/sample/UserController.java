package mybatis.generator.sample;

import io.swagger.annotations.Api;
import mybatis.generator.sample.entity.Shop;
import mybatis.generator.sample.entity.User;
import mybatis.generator.sample.mapper.ShopMapper;
import mybatis.generator.sample.mapper.UserMapper;
import mybatis.generator.sample.model.ShopQuery;
import mybatis.generator.sample.model.UserQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Created by dave on 18-5-20 上午11:46.
 */
@Api(description = "sample")
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired UserMapper userMapper;
    @Autowired ShopMapper shopMapper;

    public static class U {
        private long id;
        private String name;
        private String phone;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    @Transactional
    @GetMapping("/makeupStory")
    public List<Shop> makeupStory() {
        // 录入老板信息
        List<User> users = new ArrayList<>();
        User u = new User();
        u.setName("老板1");
        u.setPhone("11111");
        users.add(u);

        User u2 = new User();
        u2.setName("老板2");
        u2.setPhone("2222");
        users.add(u2);
        userMapper.insertAll(users);
        userMapper.insertArray(new User[]{u, u2});

        U u3 = new U();
        u3.setName("魂牵梦萦");
        u3.setPhone("3333");
        userMapper.insertU(u3);

        Map<String, Object> map = new HashMap<>();
        map.put("name", "haha");
        map.put("phone", "3344");
        userMapper.insertWithMap(map);

        Shop shop = new Shop();
        shop.setName("新华书店");
        shop.setUserId(u3.getId());
        shop.setAddress("光谷软件园");
        shopMapper.insertMany(Collections.singletonList(shop));

        UserQuery query0 = new UserQuery();
        query0.setPhone("33");
        query0.setId(Arrays.asList(3L, 4L, 5L));
        userMapper.getList(query0, "魂牵梦萦");

        ShopQuery query = new ShopQuery();
        query.setName("新华书店");
        return shopMapper.getList(query, "魂牵梦萦");
    }

    @DeleteMapping("/delete")
    public int delete() {
        UserQuery query = new UserQuery();
        query.setId(Arrays.asList(1L,2L,3L));
        query.setOffset(1);
//        query.setOffset(1);
        query.setSort("id desc, name asc");
        return userMapper.deleteByIds(Arrays.asList(1L,2L,3L));
    }
}
