package mybatis.generator.sample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by huangdachao on 2018/3/22 15:14.
 */
@Profile("dev")
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket serverApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .genericModelSubstitutes(ResponseEntity.class, DeferredResult.class)
            .useDefaultResponseMessages(false)
            .forCodeGeneration(true)
            .select().apis(RequestHandlerSelectors.basePackage("mybatis.generator"))
            .build();
    }

    /**
     * API文档名称和版本
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("")
            .version("1.0") // 版本
            .build();
    }
}
