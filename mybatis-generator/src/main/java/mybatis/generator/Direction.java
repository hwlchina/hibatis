package mybatis.generator;

/**
 * Created by huangdachao on 2018/6/21 23:03.
 */
public enum Direction {
    desc,
    asc
}
