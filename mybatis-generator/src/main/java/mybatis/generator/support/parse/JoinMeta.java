package mybatis.generator.support.parse;

import mybatis.generator.JoinType;
import mybatis.generator.annotation.Join;
import mybatis.generator.annotation.JoinColumn;

import java.util.*;

/**
 * Created by huangdachao on 2018/6/14 14:00.
 * 关联表的元数据信息
 */
public class JoinMeta {
    /**
     * 关联表表名
     */
    private Table table;
    private JoinType joinType;

    /**
     * 关联条件中引用的表名集合
     */
    private Set<Table> referTables = new HashSet<>();

    /**
     * 关联条件数组
     */
    private List<JoinConstraint> constraints = new ArrayList<>();

    public JoinMeta(Join join) {
        this.table = new Table(join.table(), join.tblAlias());
        this.joinType = join.type();
        for (JoinColumn c : join.joinColumns()) {
            this.constraints.add(new JoinConstraint(c.column(), c.referTable(), c.referTblAlias(), c.referColumn()));
            this.referTables.add(new Table(c.referTable(), c.referTblAlias()));
        }
    }

    public Table getTable() {
        return table;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public Set<Table> getReferTables() {
        return referTables;
    }

    public List<JoinConstraint> getConstraints() {
        return Collections.unmodifiableList(constraints);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JoinMeta)) return false;
        JoinMeta joinMeta = (JoinMeta) o;
        return Objects.equals(table, joinMeta.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table);
    }
}
