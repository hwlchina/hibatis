package mybatis.generator.support.parse;

import mybatis.generator.annotation.ExecuteSelect;
import mybatis.generator.annotation.GeneratedSql;
import mybatis.generator.support.Reflection;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;

/**
 * Created by huangdachao on 2018/6/22 22:47.
 */
public class MappedMethod {
    public final Class<?> mapperType;
    public final Method method;
    public final String[] views;
    public final Class<?> entityType;
    public final GeneratedSql.Type type;

    public MappedMethod(Class<?> mapperType, Method method) {
        this.mapperType = mapperType;
        this.method = method;
        this.entityType = Reflection.getEntityClass(mapperType);
        GeneratedSql generatedSql = AnnotationUtils.findAnnotation(method, GeneratedSql.class);
        type = generatedSql.type();
        ExecuteSelect es = method.getAnnotation(ExecuteSelect.class);
        if (es != null) {
            views = es.view();
        } else {
            views = new String[0];
        }
    }
}
