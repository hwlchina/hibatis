package mybatis.generator.support.parse;

/**
 * Created by huangdachao on 2018/6/14 14:48.
 * 关联表约束条件
 */
public class JoinConstraint {
    /**
     * 关联表的列
     */
    private String column;

    /**
     * 关联条件中引用的表名(非关联表表名），比如：主表名、其它关联表的表名
     */
    private String referTable;

    /**
     * 引用表别名，用于多态表的自Join
     */
    private String referTableAlias;

    /**
     * 引用表字段
     */
    private String referColumn;

    public JoinConstraint(String column, String referTable, String referTblAlias, String referColumn) {
        this.column = column;
        this.referTable = referTable;
        this.referTableAlias = referTblAlias;
        this.referColumn = referColumn;
    }

    public Table referTable() {
        return new Table(this.referTable, this.referTableAlias);
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getReferTable() {
        return referTable;
    }

    public void setReferTable(String referTable) {
        this.referTable = referTable;
    }

    public String getReferColumn() {
        return referColumn;
    }

    public void setReferColumn(String referColumn) {
        this.referColumn = referColumn;
    }
}
