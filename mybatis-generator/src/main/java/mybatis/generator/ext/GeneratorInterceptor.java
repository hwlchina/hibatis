package mybatis.generator.ext;

/**
 * Created by huangdachao on 2018/6/25 23:08.
 */
public interface GeneratorInterceptor {

    void intercept(InterceptorContext context);
}
