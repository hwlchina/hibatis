package mybatis.generator.ext;

/**
 * Created by huangdachao on 2018/6/24 23:08.
 */
public class AdditionalColumnValue {
    /**
     * 对应的表实体类字段。column优先于entityField
     */
    private String entityField;
    private String column;
    private Object val;
    private String placeholder = "";

    /**
     * 是否覆盖已有的插入值
     */
    private boolean override;

    public static AdditionalColumnValue byEntityField(String entityField, Object val) {
        AdditionalColumnValue acv = new AdditionalColumnValue(null, val);
        acv.entityField = entityField;
        return acv;
    }

    public AdditionalColumnValue(String column, Object val) {
        this.column = column;
        this.val = val;
    }

    public String getEntityField() {
        return entityField;
    }

    public void setEntityField(String entityField) {
        this.entityField = entityField;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Object getVal() {
        return val;
    }

    public void setVal(Object val) {
        this.val = val;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    @Override
    public String toString() {
        return "AdditionalColumnValue{" +
                "entityField='" + entityField + '\'' +
                ", column='" + column + '\'' +
                ", val=" + val +
                ", override=" + override +
                '}';
    }
}
