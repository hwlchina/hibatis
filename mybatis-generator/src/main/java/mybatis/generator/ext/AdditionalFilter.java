package mybatis.generator.ext;

import mybatis.generator.ConflictAction;
import mybatis.generator.FilterType;
import mybatis.generator.annotation.Filter;

/**
 * Created by huangdachao on 2018/6/24 23:03.
 */
public class AdditionalFilter {

    private String table;
    /**
     * 表别名，当有多态表需要自Join时必须指定，否则请保持为空字符串
     */
    private String tblAlias;
    private String column;
    /**
     * 对应的表实体类字段。table, column优先于entityField
     */
    private String entityField;
    private Object val;
    private String placeholder = "";
    private FilterType type = FilterType.eq;
    private int order = Filter.DEFAULT_ORDER;

    /**
     * 是否覆盖已有的过滤条件，默认不覆盖
     */
    private ConflictAction conflict = ConflictAction.discard;

    public AdditionalFilter(String table, String column, Object val) {
        this.table = table;
        this.tblAlias = "";
        this.column = column;
        this.val = val;
    }

    public AdditionalFilter(String entityField, Object val) {
        this.entityField = entityField;
        this.val = val;
    }

    public AdditionalFilter(String entityField, Object val, FilterType filterType) {
        this.entityField = entityField;
        this.val = val;
        this.type = filterType;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getTblAlias() {
        return tblAlias;
    }

    public void setTblAlias(String tblAlias) {
        this.tblAlias = tblAlias;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getEntityField() {
        return entityField;
    }

    public void setEntityField(String entityField) {
        this.entityField = entityField;
    }

    public Object getVal() {
        return val;
    }

    public void setVal(Object val) {
        this.val = val;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public FilterType getType() {
        return type;
    }

    public void setType(FilterType type) {
        this.type = type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public ConflictAction getConflict() {
        return conflict;
    }

    public void setConflict(ConflictAction conflict) {
        this.conflict = conflict;
    }

    @Override
    public String toString() {
        return "AdditionalFilter{" +
                "table='" + table + '\'' +
                ", tblAlias='" + tblAlias + '\'' +
                ", column='" + column + '\'' +
                ", entityField='" + entityField + '\'' +
                ", val=" + val +
                ", type=" + type +
                ", order=" + order +
                ", conflict=" + conflict +
                '}';
    }
}
