package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/21 23:23.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD})
@Filter(ignore = true)
public @interface Offset {

}
