package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by dave on 18-6-13 下午11:47.
 * 查询类标记，可指定过滤字段所需的公共关联表
 * 如果@Query标记与实体类@Entity标记关联了相同的表，@Entity标记优先
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Query {

    /**
     * 过滤字段所需要的关联表。如果对应的关联字段没有有效值，为了提升性能，查询时可能不关联指定的表
     * @return
     */
    Join[] join() default {};
}
