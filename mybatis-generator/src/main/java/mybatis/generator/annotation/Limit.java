package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/21 23:22.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD})
@Filter(ignore = true)
public @interface Limit {
}
