package mybatis.generator.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by dave on 18-6-14 下午11:11.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Documented
@Retention(RetentionPolicy.RUNTIME)
@GeneratedSql(type = GeneratedSql.Type.SELECT)
public @interface ExecuteSelect {
    /**
     * 返回结果视图名，可以指定多个
     * @return
     */
    String[] view() default {};

    /**
     * 限制返回结果最多只返回1条
     * @return
     */
    boolean selectOne() default false;

    @AliasFor(annotation = GeneratedSql.class, attribute = "interceptor")
    String interceptor() default "";

    /**
     * 默认过滤条件。默认过滤条件必须填写placeholder属性，placeholder可以是由表中其它column值，常量，mysql函数构成的表达式
     * @return
     */
    Filter[] filter() default {};

    /**
     * 默认排序
     * @return
     */
    String orderBy() default "";
}
