package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 17:00.
 */
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface HasMany {

    /**
     * 关联条件
     * @return
     */
    Filter[] filter() default {};

    /**
     * 关联的多条记录排序
     * @return
     */
    String orderBy() default "";

    /**
     * 最多关联的记录条数，大于0时有效
     * @return
     */
    int limit() default 0;

    /**
     * 支持的视图。查询时如果指定视图，返回结果中的字段会根据view属性进行过滤。
     * 如果指定的视图在view属性中则映射该字段，否则不映射字段。
     * 如果字段没有指定view属性或属性为空数组，则总是映射该字段。
     * 如果查询时没有指定视图，则只映射没有指定视图的字段。
     * @return
     */
    String[] view() default {};

    /**
     * 映射结果视图
     * @return
     */
    String[] resultView() default {};
}
