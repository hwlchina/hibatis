package mybatis.generator.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/15 11:23.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Documented
@Retention(RetentionPolicy.RUNTIME)
@GeneratedSql(type = GeneratedSql.Type.DELETE)
public @interface ExecuteDelete {

    @AliasFor(annotation = GeneratedSql.class, attribute = "interceptor")
    String interceptor() default "";

    /**
     * 默认过滤条件。默认过滤条件必须填写placeholder属性，placeholder可以是由表中其它column值，常量，mysql函数构成的表达式
     * @return
     */
    Filter[] filter() default {};
}
