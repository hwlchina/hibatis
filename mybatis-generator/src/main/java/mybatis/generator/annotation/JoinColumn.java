package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 18:43.
 * 关联表约束条件
 */
@Target(ElementType.ANNOTATION_TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface JoinColumn {

    /**
     * 关联表列名
     * @return
     */
    String column();

    /**
     * 关联条件引用的表名，为空时使用实体类主表名
     * @return
     */
    String referTable() default "";

    /**
     * 引用表别名，用于多态表的自Join
     * @return
     */
    String referTblAlias() default "";

    /**
     * 关联条件引用表的列名
     * @return
     */
    String referColumn();
}
