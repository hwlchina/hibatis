package mybatis.generator.annotation;

import mybatis.generator.JoinType;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 16:38.
 */
@Target(ElementType.ANNOTATION_TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Join {
    /**
     * 关联表表名
     * @return
     */
    String table();

    /**
     * 关联表别名，用于多态表的自Join
     * @return
     */
    String tblAlias() default "";

    JoinType type() default JoinType.inner;

    /**
     * 关联约束条件数组
     * @return
     */
    JoinColumn[] joinColumns();
}
