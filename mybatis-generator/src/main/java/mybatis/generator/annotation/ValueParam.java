package mybatis.generator.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/15 15:13.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
public @interface ValueParam {

    @AliasFor("field")
    String value() default "";

    /**
     * 对应的实体类字段名
     * @return
     */
    @AliasFor("value")
    String field() default "";

    /**
     * 表列名。优先级高于field。
     * @return
     */
    String column() default "";

    /**
     * sql语句参数占位符，比如 "year(#{$@})"，这里的'$@'将被字段名替换
     * @return
     */
    String placeholder() default "";

    /**
     * column不为空时有效
     * 当指定column时，表示参数值为null时，是否忽略本参数
     * 没指定column时，参数为null时，总是忽略本参数
     * @return
     */
    boolean ignoreOnNull() default false;

    /**
     * column不为空时有效
     * 对于Number或int, long, float, double类型参数，是否在字段值为0时忽略本参数
     * @return
     */
    boolean ignoreOnZero() default false;

    /**
     * 当相同的TableColumn字段值存在时的行为
     * @return
     */
    boolean override() default true;
}
