package mybatis.generator.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/14 17:02.
 */
@Target(ElementType.ANNOTATION_TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneratedSql {
    enum Type {
        INSERT, UPDATE, DELETE, SELECT, COUNT
    }

    /**
     * 生成语句类型
     * @return
     */
    Type type();

    /**
     * 拦截器，必须是当前Mapper接口的default方法或静态方法
     * 方法签名：GeneratorInterceptor.interceptor
     * @return
     */
    String interceptor() default "";
}
