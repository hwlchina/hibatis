package mybatis.generator.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by dave on 18-6-14 下午8:22.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Generated
public @interface Id {

    /**
     * 对应的数据库列名
     * @return
     */
    String column() default "";

    /**
     * 是否数据库自动生成主键
     * @return
     */
    @AliasFor(annotation = Generated.class, attribute = "value")
    boolean useGeneratedKeys() default true;

    /**
     * 是否可插入
     * @return
     */
    boolean insertable() default false;

    /**
     * 是否可更新
     * @return
     */
    boolean updateable() default false;
}
