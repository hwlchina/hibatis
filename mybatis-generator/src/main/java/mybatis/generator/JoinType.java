package mybatis.generator;

/**
 * Created by dave on 18-6-30 上午12:18.
 */
public enum JoinType {
    inner, left, right
}
