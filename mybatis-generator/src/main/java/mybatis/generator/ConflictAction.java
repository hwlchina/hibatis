package mybatis.generator;

/**
 * Created by huangdachao on 2018/7/3 15:16.
 */
public enum ConflictAction {
    override,    // 覆盖
    discard,     // 忽略
    coexist,     // 同时保留
}
