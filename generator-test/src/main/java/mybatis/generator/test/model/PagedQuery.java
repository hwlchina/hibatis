package mybatis.generator.test.model;

import mybatis.generator.annotation.Limit;
import mybatis.generator.annotation.Offset;

/**
 * Created by dave on 18-7-1 下午4:38.
 */
public class PagedQuery {
    @Limit
    private long pageSize = 20;

    private long pageNo = 1;

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getPageNo() {
        return pageNo;
    }

    public void setPageNo(long pageNo) {
        this.pageNo = pageNo;
    }

    @Offset
    public long getOffset() {
        return (pageNo - 1) * pageSize;
    }
}
