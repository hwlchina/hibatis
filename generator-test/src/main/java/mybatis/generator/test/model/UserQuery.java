package mybatis.generator.test.model;

import mybatis.generator.FilterType;
import mybatis.generator.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by dave on 18-7-1 下午4:37.
 */
@Query(join = @Join(table = "car", joinColumns = @JoinColumn(column = "driver_id", referTable = "user", referColumn = "id")))
public class UserQuery extends PagedQuery {

    @Filter(type = FilterType.preMatch)
    private String name;

    @Filter(type = FilterType.preMatch)
    private String phone;

    private Integer type;

    @Filter(type = FilterType.in)
    private List<Long> companyId;

    @Filter(type = FilterType.in)
    private List<Long> bossId;

    @Filter(table = "car", type = FilterType.preMatch)
    private String license;

    private String bossName;

    @Filter(type = FilterType.preMatch)
    private String companyName;

    @Filter(field = "createTime", type = FilterType.goe, placeholder = "($(car).create_time)")
    private Boolean createBeforeCar;

    @Filter(field = "type", placeholder = "(1)")
    private Boolean onlyStaff;

    @Filter(table = "car", column = "create_time", type = FilterType.loe, placeholder = "DATE_SUB(NOW(), INTERVAL #{$@} DAY)")
    private Integer carUsedDays; // 车辆使用天数大于

    @Sort
    private String sort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Long> getCompanyId() {
        return companyId;
    }

    public void setCompanyId(List<Long> companyId) {
        this.companyId = companyId;
    }

    public List<Long> getBossId() {
        return bossId;
    }

    public void setBossId(List<Long> bossId) {
        this.bossId = bossId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getBossName() {
        return bossName;
    }

    public void setBossName(String bossName) {
        this.bossName = bossName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getCreateBeforeCar() {
        return createBeforeCar;
    }

    public void setCreateBeforeCar(Boolean createBeforeCar) {
        this.createBeforeCar = createBeforeCar;
    }

    public Boolean getOnlyStaff() {
        return onlyStaff;
    }

    public void setOnlyStaff(Boolean onlyStaff) {
        this.onlyStaff = onlyStaff;
    }

    public Integer getCarUsedDays() {
        return carUsedDays;
    }

    public void setCarUsedDays(Integer carUsedDays) {
        this.carUsedDays = carUsedDays;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
