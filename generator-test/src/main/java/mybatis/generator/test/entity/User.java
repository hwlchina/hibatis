package mybatis.generator.test.entity;

import mybatis.generator.JoinType;
import mybatis.generator.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by dave on 18-6-30 上午11:01.
 */
@Entity(table = "user", join = {@Join(table = "company", type = JoinType.left, joinColumns = @JoinColumn(column = "id", referColumn = "company_id", referTable = "user")),
    @Join(table = "user", type = JoinType.left, tblAlias = "boss", joinColumns = @JoinColumn(column = "id", referTable = "user", referColumn = "boss_id"))})
public class User {
    @Id
    private long id;

    private String name;

    private String phone;

    private int type;       // 1：员工，2：老板，3：管理员

    private long companyId;

    private long bossId;

    @Column(view = {"withCompany", "detail"}, table = "company", column = "name")
    private String companyName;

    @Column(view = {"withCompany", "detail"}, table = "user", tblAlias = "boss", column = "name")
    private String bossName;

    @HasOne(view = "detail", filter = @Filter(column = "id", placeholder = "companyId", ignoreOnZero = true))
    private Company company;

    @HasOne(view = "detail", filter = @Filter(column = "id", placeholder = "bossId", ignoreOnZero = true))
    private User boss;

    @HasMany(view = "detail", filter = @Filter(column = "driver_id", placeholder = "id"))
    private List<Car> car;

    @HasMany(view = "detail", limit = 5, orderBy = "id desc", filter = @Filter(column = "user_id", placeholder = "id"))
    private List<LoginLog> logins;

    private Date createTime;

    private Date updateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getBossId() {
        return bossId;
    }

    public void setBossId(long bossId) {
        this.bossId = bossId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBossName() {
        return bossName;
    }

    public void setBossName(String bossName) {
        this.bossName = bossName;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getBoss() {
        return boss;
    }

    public void setBoss(User boss) {
        this.boss = boss;
    }

    public List<Car> getCar() {
        return car;
    }

    public void setCar(List<Car> car) {
        this.car = car;
    }

    public List<LoginLog> getLogins() {
        return logins;
    }

    public void setLogins(List<LoginLog> logins) {
        this.logins = logins;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
