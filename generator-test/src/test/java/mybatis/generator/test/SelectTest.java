package mybatis.generator.test;

import mybatis.generator.test.entity.User;
import mybatis.generator.test.mapper.UserMapper;
import mybatis.generator.test.model.UserQuery;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by dave on 18-6-29 下午11:12.
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SelectTest {
    @Autowired UserMapper userMapper;

    @Test
    public void selectList() {
        UserQuery query = new UserQuery();
        query.setBossName("玄宗");
        query.setLicense("T10");
        query.setOnlyStaff(true);
        query.setCarUsedDays(1);
        query.setCreateBeforeCar(true);
        query.setSort("createTime desc, id asc"); // 直接引用实体类字段
        List<Map> users = userMapper.getList(query);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void selectListOrderedByCreateTime() {
        UserQuery query = new UserQuery();
        query.setBossName("玄宗");
        query.setSort("$[companyName] desc");  // 使用Field Expression
        List<Map> users = userMapper.getList(query);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void selectListOrderedByCarCreateTime() {
        UserQuery query = new UserQuery();
        query.setBossName("玄宗");
        query.setSort("$(car).create_time desc"); // 使用Table Expression
        List<Map> users = userMapper.getList(query);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void search() {
        List<User> list = userMapper.search(1, "T10");
        Assert.assertTrue(list.size() > 0);
        Assert.assertTrue(list.get(0).getId() > list.get(1).getId());
    }

    @Test
    public void searchByType() {
        List<User> list = userMapper.selectByType(null);
        Assert.assertTrue(list.size() > 5);
        list = userMapper.selectByType(2);
        Assert.assertEquals(list.size(), 2);
    }

    @Test
    public void selectCount() {
        UserQuery query = new UserQuery();
        query.setBossName("玄宗");
        query.setLicense("T10");
        Assert.assertTrue(userMapper.selectCount(query) > 0);
    }

    @Test
    public void selectOne() {
        UserQuery query = new UserQuery();
        query.setBossName("玄宗");
        query.setLicense("T10");
        Assert.assertNotNull(userMapper.selectOne(query));
    }

    @Test
    public void selectById() {
        User user = userMapper.getById(3);
        Assert.assertEquals(user.getCompanyName(), "唐");
        Assert.assertEquals(user.getBossName(), "玄宗");
        Assert.assertEquals(user.getCompany().getName(), "唐");
        Assert.assertEquals(user.getBoss().getName(), "玄宗");
        Assert.assertTrue(user.getLogins().size() > 0);
        Assert.assertTrue(user.getCar().size() > 0);
        Assert.assertTrue(user.getLogins().size() > 0);
    }

    @Test
    public void selectByIds() {
        List<User> users = userMapper.getByIds(Arrays.asList(3L, 4L));
        Assert.assertEquals(users.size(), 2);
        Assert.assertEquals(users.get(0).getId(), 3L);
        Assert.assertEquals(users.get(0).getCompanyName(), "唐");
        Assert.assertEquals(users.get(0).getCompany().getName(), "唐");
        Assert.assertEquals(users.get(1).getId(), 4L);
        Assert.assertEquals(users.get(1).getCompanyName(), "唐");
        Assert.assertEquals(users.get(1).getCompany().getName(), "唐");
    }

    @Test
    public void searchByCondition() {
        Map<String, Object> map = new HashMap<>();
        map.put("bossName", "玄宗");
        List<User> list = userMapper.searchByCondition(map, 2);
        Assert.assertTrue(list.size() > 0);
    }
}
