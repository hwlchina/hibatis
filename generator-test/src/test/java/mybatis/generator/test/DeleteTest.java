package mybatis.generator.test;

import mybatis.generator.test.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * Created by dave on 18-7-5 下午8:29.
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DeleteTest {
    @Autowired UserMapper userMapper;

    @Test
    public void deleteById() {
        userMapper.deleteById(1L);
        Assert.assertNull(userMapper.getById(1L));
    }

    @Test
    public void deleteByIds() {
        userMapper.deleteByIds(Arrays.asList(1L, 2L));
        Assert.assertNull(userMapper.getById(1L));
    }

    @Test
    public void deleteByCondition() {
        userMapper.deleteByCondition(null);
        Assert.assertNull(userMapper.getById(1L));
    }

    @Test
    public void deleteByCompanyName() {
        int res = userMapper.deleteByCompanyName("唐");
        Assert.assertEquals(res, 5);
        Assert.assertNull(userMapper.getById(3L));
    }
}
