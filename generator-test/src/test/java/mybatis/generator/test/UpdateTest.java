package mybatis.generator.test;

import mybatis.generator.test.entity.User;
import mybatis.generator.test.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * Created by huangdachao on 2018/7/5 15:57.
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UpdateTest {
    @Autowired UserMapper userMapper;

    @Test
    public void changeName() {
        userMapper.changeName(3, "乐乐");
        User u = userMapper.getById(3);
        Assert.assertEquals(u.getName(), "乐乐");
    }

    @Test
    public void changeNames() {
        userMapper.changeNames(Arrays.asList(3L, 4L), "乐乐");
        User u = userMapper.getById(3);
        Assert.assertEquals(u.getName(), "乐乐");
    }

    @Test
    public void changeAllName() {
        userMapper.changeAllName("乐乐");
        User u = userMapper.getById(3);
        Assert.assertEquals(u.getName(), "乐乐");
    }

    @Test
    public void update() {
        User user = userMapper.getById(3);
        user.setPhone("11122223333");
        userMapper.update(user);
        Assert.assertEquals(userMapper.getById(3).getPhone(), "11122223333");
    }
}
