package mybatis.generator.test;

import mybatis.generator.test.entity.User;
import mybatis.generator.test.mapper.UserMapper;
import mybatis.generator.test.model.UserQuery;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by huangdachao on 2018/7/5 15:52.
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class InsertTest {
    @Autowired UserMapper userMapper;

    @Test
    public void insert() {
        ArrayList<User> users = new ArrayList<>();
        User u = new User();
        u.setName("小东");
        u.setPhone("13312345678");
        u.setBossId(8);
        u.setCompanyId(1);
        u.setType(1);
        users.add(u);

        u = new User();
        u.setName("小六");
        u.setPhone("13212345678");
        u.setBossId(9);
        u.setCompanyId(2);
        u.setType(1);
        users.add(u);
        userMapper.insertList(users);

        UserQuery query = new UserQuery();
        query.setCompanyId(Collections.singletonList(1L));
        Assert.assertEquals(userMapper.selectCount(query), 7);
    }

    @Test
    public void insertMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "小东");
        map.put("phone", "123456789123");
        map.put("bossId", 8);
        map.put("companyId", 1);
        map.put("type", 1);
        map.put("abc", "def");
        userMapper.insertMap(map);

        UserQuery query = new UserQuery();
        query.setCompanyId(Collections.singletonList(1L));
        Assert.assertEquals(userMapper.selectCount(query), 7);
    }
}
